import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styles: [`
  .hidden-msg{
    display: none;
  }
  `
  ]
})
export class AddComponent implements OnInit {

  msgName: string = 'Message of Component';
  msgColor: string = '';

  miForm: FormGroup = this.fb.group({
    name: ['', Validators.required]
  });
  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
  }

  changeName() {
    this.msgName = 'Text msg change info...';
  }

  changeColor() {
    const color = '#xxxxxx'.replace(/x/g, y => (Math.random() * 16 | 0).toString(16));
    this.msgColor = color;
  }
}
