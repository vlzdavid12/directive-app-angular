import {Directive, ElementRef, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Directive({
  selector: '[error-msg]'
})
export class ErrorMsgDirective implements OnInit, OnChanges {

  private _text: string = 'Text of directive';
  private _color: string = 'red';

  htmlElement: ElementRef<HTMLElement>;

  @Input() set color(value: string){
    this.htmlElement.nativeElement.style.color = value;
    this._color =  value;
  }
  @Input() set text(value: string ){
    this.htmlElement.nativeElement.innerText = value;
    this._text =  value;
  }

  @Input() set validate(value: boolean ){
   if (value){
     this.htmlElement.nativeElement.classList.add('hidden-msg');
   }else {
     this.htmlElement.nativeElement.classList.remove('hidden-msg');
   }
  }

  constructor(private el: ElementRef<HTMLElement>) {
    this.htmlElement = el;
  }

  ngOnChanges(params: SimpleChanges) {
   /* const {text, color} = params;
    if (text) {
      this.htmlElement.nativeElement.innerText = text.currentValue;
    }
    if (color) {
      this.htmlElement.nativeElement.style.color = color.currentValue;
    }*/
  }

  ngOnInit() {
     this.setColor();
     this.setTxt();
     this.setStyle();
  }

  setStyle(): void {
    this.htmlElement.nativeElement.classList.add('form-text');
  }

  setColor(): void{
    this.htmlElement.nativeElement.style.color = this._color;
  }

  setTxt(): void{
    this.htmlElement.nativeElement.innerText = this._text;
  }


}
