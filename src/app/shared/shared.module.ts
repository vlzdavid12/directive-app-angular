import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorMsgDirective } from './directive/error-msg.directive';
import { CustomIfDirective } from './directive/custom-if.directive';



@NgModule({
  declarations: [
    CustomIfDirective,
    ErrorMsgDirective,
  ],
  exports: [
    CustomIfDirective,
    ErrorMsgDirective
  ],
  imports: [
    CommonModule
  ]
})
export class SharedModule { }
